function serviceHandler(req, res, promise) {
	promise
	.then((data) => {
		res.send(data);
	})
	.catch((err) => {
		res.status(400).send(err);
	});
};


module.exports = serviceHandler;