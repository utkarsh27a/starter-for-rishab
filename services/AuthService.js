var BaseService = require("./BaseService");
var UserModel   = require("../models/User");

class AuthService extends BaseService {
    login(email, password) {
        return UserModel.findByCredentials(email, password);
    }

}

module.exports = AuthService;