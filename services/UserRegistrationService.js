var BaseService = require("./BaseService");
var UserModel 	= require("../models/User");


class UserRegistrationService extends BaseService {
	register(name, email, password) {
		return UserModel.create({name, email, password});
	}

	list() {
		return UserModel.find();
	}
}

module.exports = UserRegistrationService;