var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const uuid = require('uuid');
const bcrypt = require("bcryptjs")
const config = require("../config")


var UserSchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  email:  {
    type: String,
    required: true
  },
  password:  {
    type: String,
    required: true
  }
}, {
  timestamps: true
});


UserSchema.methods.generateAuthToken = function() {
  var user = this;
  var token = jwt.sign(JSON.stringify({_id : user._id.toHexString()}), config.jwtSecret).toString();
  return Promise.resolve({
    token
  });
};

UserSchema.methods.toJSON = function() {
  var user = this;
  var userObject = user.toObject();
  return _.pick(user, [ "_id", "name","email"]);
}

UserSchema.statics.findByCredentials = function(email, password) {
  var User = this;
  return User.findOne({
    email
  }).then((user) => {
    if (!user) {
      return Promise.reject("Your E-mail is incorrect.");
    }
    if (bcrypt.compareSync(password, user.password)) {
      return user.generateAuthToken().then((token) => {
        return {token, user};
      });
    }
    return Promise.reject("Your Password is incorrect.");
  });
};

UserSchema.statics.findByToken = function(token) {
  var User = this;
  var decode;
  try {
      decode = jwt.verify(token, config.jwtSecret);
  }catch (e) {
    return Promise.reject(e)
  }
  return User.findOne({
    "_id": decode._id
  });
};

UserSchema.pre("save", function(next) {
  var user = this;
  if (user.isModified("password")) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, function(err, hash) {
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

var UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;
