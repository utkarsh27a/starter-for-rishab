var express = require('express');
var router = express.Router();
var serviceHandler =  require('../serviceHandler');
var UserRegistrationService =  require('../services/UserRegistrationService');
var AuthService =  require('../services/AuthService');


router.use('/', function(req, res, next) {
	next();
});

router.post('/login', function(req, res, next) {
  	var authServiceInst = new AuthService();
	serviceHandler(req, res, authServiceInst.login(req.body.email, req.body.password));
});

router.get('/user', function(req, res, next) {
 	var userRegistrationServiceInst = new UserRegistrationService();
	serviceHandler(req, res, userRegistrationServiceInst.list(req.body.name, req.body.email, req.body.password));
});

router.post('/user', function(req, res, next) {
	var userRegistrationServiceInst = new UserRegistrationService();
	serviceHandler(req, res, userRegistrationServiceInst.register(req.body.name, req.body.email, req.body.password));
});

module.exports = router;
