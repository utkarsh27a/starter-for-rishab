var server = require("./server.json");
var jwt = require("./jwt.json");

var config = {
	port: server.port,
	jwtSecret: jwt.secret
};

module.exports = config;